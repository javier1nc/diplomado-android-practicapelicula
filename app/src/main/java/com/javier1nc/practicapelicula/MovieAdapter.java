package com.javier1nc.practicapelicula;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Tazmadroid on 27/01/18.
 */

public class MovieAdapter extends ArrayAdapter<Movie> {

  private Context context=null;
  private List<Movie> movies=null;

  public MovieAdapter(@NonNull Context context,
                      @NonNull List<Movie> objects) {
    super(context, R.layout.item_movie, objects);
    this.movies=objects;
    this.context=context;
  }

  @NonNull
  @Override
  public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
    View item=convertView;
    MovieViewHolder movieViewHolder;
    if(item==null){
      LayoutInflater inflater=(LayoutInflater) context.
          getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      item=inflater.inflate(R.layout.item_movie,null);
      movieViewHolder= new MovieViewHolder();
      movieViewHolder.movieImageView =(ImageView) item
          .findViewById(R.id.contactImageView);
      movieViewHolder.tituloTextView=(TextView) item
          .findViewById(R.id.tituloTextView);
      movieViewHolder.generoTextView =(TextView) item
          .findViewById(R.id.generoTextView);
      movieViewHolder.publicacionTextView =(TextView) item
              .findViewById(R.id.publicacionTextView);
      movieViewHolder.rankingTextView =(TextView) item
              .findViewById(R.id.rankingTextView);
      movieViewHolder.precioTextView =(TextView) item
              .findViewById(R.id.precioTextView);


      item.setTag(movieViewHolder);
    }else{
      movieViewHolder=(MovieViewHolder) item.getTag();
    }


    movieViewHolder.movieImageView.setImageResource(R.mipmap.movies);
    movieViewHolder.tituloTextView.setText(movies.get(position).getTitulo());
    movieViewHolder.generoTextView.setText(movies.get(position).getGenero());
    movieViewHolder.publicacionTextView.setText(movies.get(position).getPublicacion());
    movieViewHolder.rankingTextView.setText(movies.get(position).getRanking());
    movieViewHolder.precioTextView.setText(movies.get(position).getPrecio());
    return item;
  }

  @Override
  public int getCount() {
    return movies.size();
  }

  public Movie getContactSelected(int position){
    return movies.get(position);
  }

  static class MovieViewHolder {
    ImageView movieImageView;
    TextView tituloTextView;
    TextView generoTextView;
    TextView publicacionTextView;
    TextView rankingTextView;
    TextView precioTextView;
    TextView descriptionTextView;
  }
}
