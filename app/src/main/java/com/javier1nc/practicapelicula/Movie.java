package com.javier1nc.practicapelicula;

/**
 * Created by Tazmadroid on 27/01/18.
 */

public class Movie {

  private int id=0;
  private String titulo=null;
  private String genero=null;
  private String publicacion=null;
  private String ranking=null;
  private String precio=null;
  private String description=null;
  private int age=0;

  public Movie() {
  }

  public Movie(int id, String titulo, String genero, String publicacion, String ranking, String precio, String description) {
    this.id = id;
    this.titulo = titulo;
    this.genero = genero;
    this.publicacion = publicacion;
    this.ranking = ranking;
    this.precio = precio;
    this.description = description;
  }


  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getTitulo() {
    return titulo;
  }

  public void setTitulo(String name) {
    this.titulo = name;
  }

  public String getGenero() { return genero;}

  public void setGenero(String genero) {
    this.genero = genero;
  }

  public String getPublicacion() {
    return publicacion;
  }

  public void setPublicacion(String publicacion) {
    this.publicacion = publicacion;
  }

  public String getRanking() { return ranking;}

  public void setRanking(String ranking) {
    this.ranking = ranking;
  }

  public String getPrecio() { return precio;}

  public void setPrecio(String precio) {
    this.precio = precio;
  }

  public String getDescription() { return description;}

  public void setDescription(String description) {
    this.description = description;
  }







  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Movie movie = (Movie) o;

    if (id != movie.id) return false;
    if (!titulo.equals(movie.titulo)) return false;
    if (genero != null ? !genero.equals(movie.genero) : movie.genero != null)
      return false;
    if (publicacion != null ? !publicacion.equals(movie.publicacion) : movie.publicacion != null)
      return false;
    return age != movie.age;
  }

  @Override
  public int hashCode() {
    int result = id;
    result = 31 * result + titulo.hashCode();
    result = 31 * result + (genero != null ? genero.hashCode() : 0);
    result = 31 * result + (publicacion != null ? publicacion.hashCode() : 0);
    result = 31 * result + (description != null ? description.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "Movie{" +
        "id=" + id +
        ", titulo='" + titulo + '\'' +
        ", genero='" + genero + '\'' +
        ", publicacion='" + publicacion + '\'' +
        ", ranking='" + ranking + '\'' +
        ", ranking='" + precio + '\'' +
        ", description='" + description + '\'' +
        ", age='" + age + '\'' +
        '}';
  }
}
