package com.javier1nc.practicapelicula;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView movies=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        movies=(ListView) findViewById(R.id.moviesListView);
        MovieAdapter movieAdapter = new MovieAdapter(
                getBaseContext(),
                getMovies());
        movies.setAdapter(movieAdapter);

        movies.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getBaseContext(),
                        ((MovieAdapter)parent.getAdapter()).getContactSelected(position).getDescription(),
                        Toast.LENGTH_LONG).show();
            }
        });


    }

    public List<Movie> getMovies(){
        List<Movie> movies = new ArrayList<>();
        movies.add(new Movie(1,"Matrix ","Accion", "1999", "8", "350 MXN","Description: Matrix 1"));
        movies.add(new Movie(2,"Matrix 2","Accion", "2011", "8", "350 MXN","Description: Matrix 2"));
        movies.add(new Movie(3,"Matrix 3","Accion", "2012", "8", "350 MXN","Description: Matrix 3"));
        movies.add(new Movie(4,"Stars Wars","Aventura", "1974", "8", "350 MXN","Description: Stars Wars"));
        movies.add(new Movie(5,"Stars Wars II","Aventura", "1980", "8", "350 MXN","Description: Stars Wars II"));
        movies.add(new Movie(6,"Stars Wars III","Aventura", "1981", "8", "350 MXN","Description: Stars Wars III"));
        movies.add(new Movie(7,"Stars Wars IV","Aventura", "2018", "8", "350 MXN","Description: Stars Wars IV"));
        movies.add(new Movie(8,"Stars Wars V","Aventura", "2018", "8", "350 MXN","Description: Stars Wars V"));
        movies.add(new Movie(9,"Stars Wars VII","Aventura", "2018", "8", "350 MXN","Description: Stars Wars VII"));
        movies.add(new Movie(10,"Stars Wars VIII","Aventura", "2018", "8", "350 MXN","Description: Stars Wars VIII"));
        movies.add(new Movie(11,"Stars Wars IX","Aventura", "2018", "8", "350 MXN","Description: Stars Wars IX"));
        movies.add(new Movie(12,"Stars Wars X","Aventura", "2018", "8", "350 MXN","Description: Stars Wars X"));
        movies.add(new Movie(13,"Harry Potter","Aventura", "2018", "8", "350 MXN","Description: Harry Potte"));
        movies.add(new Movie(14,"Harry Potter 2","Aventura", "2018", "8", "350 MXN","Description: Harry Potter 2"));
        movies.add(new Movie(15,"Harry Potter 3","Aventura", "2018", "8", "350 MXN","Description: Harry Potter 3"));
        movies.add(new Movie(16,"Harry Potter 4","Aventura", "2018", "8", "350 MXN","Description: Harry Potter 4"));
        movies.add(new Movie(17,"Harry Potter 5","Aventura", "2018", "8", "350 MXN","Description: Harry Potter 5"));
        movies.add(new Movie(18,"Volver al Futuro","CiFi", "2018", "8", "350 MXN","Description: Volver al Futuro"));
        movies.add(new Movie(19,"Volver al Futuro 2","CiFi", "2018", "8", "350 MXN","Description: Volver al Futuro 2"));
        movies.add(new Movie(20,"Volver al Futuro 3","CiFi", "2018", "8", "350 MXN","Description: Volver al Futuro 3"));
        return movies;
    }
}
